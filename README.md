<h1 align="center"><i>Project</i> Tugas Akhir SIB Indobot Academy - Kelompok 4 (<b>&#128680;</b>)</h1>

<p align="center"><i >16 February - 16 Juni 2023</i></p>

> ## Info 
> Untuk kenyamanan dalam membaca dokumentasi ini, baca panduan singkat berikut:
> + gunakan shortcut `ctrl + click` ketika menemukan link atau tautan dalam dokumen ini. *Shortcut* ini akan membantu anda menjelajahi kontent pada *link* tanpa menimpa dokumentasi ini.
> + Kami menyediakan *footnotes* pada beberapa kalimat yang akan mengantarkan anda ke baris paling bawah dokumen. Jika anda ingin kembali anda cukup menekan tanda panah `backspace` pada ujung kalimat *footnotes*.
> + Gunakan daftar isi untuk berpindah antar Judul atau subjudul.

**Nama Anggota:**
|Foto Profile|Nama|Perguruan Tinggi|Job Desk|
|----|---|---|---|
||Yudan Aloya Pagalo|||
||Azma Syarif|||
||Japar Sodik|||
||Samuel Manihuruk|||
||Tri Ilman A. Fattah|Universitas Islam Sultan Agung|Software Engineer|

-----

> ### Daftar isi
> + [Produk 1: ZabutON (Smart Seat Cushion)](#produk1)
>   + [Intro](#intro)
>   + [Desain Produk](#desain-produk)
>   + [Komponen Elektronik](#komponen-elektronik)
>   + [Komponen Mekanik](#komponen-mekanik)
> + [Produk Dua: Terminal LCD](#produk2)
> + [LISENSI](#lisensi)

----

<h2 id="produk1"> Produk 1: ZabutON (Smart Seat Cushion)</h2>

## Intro 
### Filosofi Nama Produk
        
**ZabutON** berasal dari bahasa Jepang **ざぶとん (zabuton)** yang jika diterjemahkan dalam bahasa Inggris yaitu *seat cushion* atau bantal yang dipakai untuk duduk. Mengapa huruf `ON` ditulis kapital? Ini menunjukan bahwa bantalan dudukan yang kami buat dalam project IoT ini bukan bantal dudukan biasa, tetapi bantal dudukan yang dilengkapi perangkat elektronik yang dapat mentransmisikan data biner. Produk ini hanya dapat dirasakan manfaatnya ketika dalam mode `ON`.

### Latar Belakang

Berikut analisis yang dilakukan AI terhadap dampak yang diakibatkan [duduk dalam jangka waktu yang lama.][analisis1] 
Berikut artikel yang menguatkan dampak dari durasi waktu duduk yang lama terhadap kesehatan tubuh.[^1]

[^1]: [What are the risks of sitting too much?](https://www.mayoclinic.org/healthy-lifestyle/adult-health/expert-answers/sitting/faq-20058005)

[analisis1]: https://labs.kagi.com/fastgpt?query=dampak+kelamaan+saat+duduk 

### Tujuan

Tujuan dari porduk ini tidak lain adalah untuk menyehatkan masyarakat Indonesia secara khusus dan masyarakat Internasional secara umum. Ini spesial juga bagi para *nerd*, *geek*, *gammer*, workaholic, serta kaum rebahan yang susah untuk mengatur waktu duduk dan tidur agar lebih sering untuk beraktivitas di luar terlepas dari kesibukan yang ada.   

## Desain Produk

## Komponen Elektronik
No.|Bahan|Jenis|Dokumentasi|Gambar|Harga|
|---|----|:-----:|-----|:-----:|----:|
|1.1|[*Force Sensitive Resistor/Thin Film Pressure Flexible*](https://shorturl.at/gjCZ4 "Lihat Definisinya via chatGPT")|Sensor|[link 1](https://www.instructables.com/How-to-use-a-Force-Sensitive-Resistor-Arduino-Tuto/)\>[link 2](https://manuals.plus/id/fsr/fsr-force-sensing-resistor-integration-guide-and-evaluation-parts-catalog#force_sensing_resistorsr-_an_overview_of_the_technology)|<div align="center"><a href="https://shorturl.at/hKR18"><img src="https://bc-robotics.com/wp-content/uploads/2013/05/fsr1-400x400.jpg" width="200px" title="size: 19mm. Harga: Rp 119.900"></a></div>|Rp 119.900|
|1.2|[*Piezoelectric*](https://shorturl.at/BDX36)|Sensor||<a href="https://www.tokopedia.com/archive-pusatfashionnn/better-10pcs-disk-drum-trigger-sensor-buzzer-piezo-element-20mm-kabel?extParam=fcity%3D165%26ivf%3Dfalse%26shop_tier%3D3%26src%3Dsearch"><img src="https://bc-robotics.com/wp-content/uploads/2019/06/piezo3-400x400.jpg" width="200px" title="size: 20mm. Harga: Rp 59.800/10pcs"></a>|Rp 59.800|
|2|*Tp4056*|charging module||<a href="https://shorturl.at/hINW0"><img src="https://shorturl.at/anEGR" width="200px" title="Harga: Rp 4.500"></a>|Rp 4.500|
|3|*Lip battery*|Battery||<a href="https://shorturl.at/vwY69"><img src="https://shorturl.at/gBQ06" width="200px" title="Voltase: 3.7v. Harga: Rp 65.000"></a>|Rp 65.000|
|4|*Wemos D1 Mini*|Development Board||<a href="https://shorturl.at/cejZ4"><img src="https://shorturl.at/hruD3" width="200px" title="Harga: Rp 27.500"></a>|Rp 27.500|

## Komponen Mekanik


<h2 id="produk2">Produk Dua: Terminal LCD</h2>

## LISENSI
ZabutON is released under the [MIT License.][lisensi]

[lisensi]: ./LICENSE
